using Idpwmemo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace IdpwmemoTest
{
    [TestClass]
    public class UnitTest1
    {
        // Cryptor.Version == 1 のデータ
        [TestMethod]
        public void TestReadV1()
        {
            IDPWMemo memo = new IDPWMemo();
            memo.SetPassword(Src.Password);
            memo.LoadMemo(Src.Sample1Memo);
            check(memo);
            byte[] data = memo.Save();
            // Memo.Versionが異なるので不一致(memo.Save()はMemo.Version=2, Src.Sample1MemoはMemo.Version=1)
            CollectionAssert.AreNotEqual(Src.Sample1Memo, data);

            memo.ConvertV1ToV2();
            byte[] data2 = memo.Save();
            CollectionAssert.AreEqual(Src.Sample2Memo, data2);
        }

        // Cryptor.Version == 2 のデータ
        [TestMethod]
        public void TestReadV2()
        {
            IDPWMemo memo = new IDPWMemo();
            memo.SetPassword(Src.Password);
            memo.LoadMemo(Src.Sample2Memo);
            check(memo);
            byte[] data = memo.Save();
            // Memo.Versionも一致するのでデータが一致
            CollectionAssert.AreEqual(Src.Sample2Memo, data);
        }

        [TestMethod]
        public void TestWrite()
        {
            IDPWMemo memo = new IDPWMemo();
            memo.SetPassword(Src.Password);
            memo.NewMemo();
            foreach (ServiceSrc src in Src.ServiceSetList)
            {
                memo.AddNewService(src.Name);
                List<Value> values = memo.GetValues();
                foreach (Value v in src.Values)
                {
                    if (v.Type == ValueType.ServiceName)
                    {
                        continue;
                    }
                    values.Add(v);
                }
                List<Value> secrets = memo.GetSecrets();
                foreach (Value v in src.Secrets)
                {
                    secrets.Add(v);
                }
                memo.UpdateSelectedService();
            }
            // テストデータのTimeは0なので一致確認のためだけに揃える
            foreach (Service sv in memo.Services)
            {
                sv.Time = 0;
            }
            check(memo);
            byte[] data = memo.Save();
            CollectionAssert.AreEqual(Src.Sample2Memo, data);
        }

        [TestMethod]
        public void TestChangePassword()
        {
            IDPWMemo memo = new IDPWMemo();
            memo.SetPassword(Src.Password);
            memo.LoadMemo(Src.Sample2Memo);

            byte[] oldPassword = Src.Password;
            byte[] newPassword = System.Text.Encoding.UTF8.GetBytes("ふぅバァＢＡＺ！");

            memo.ChangePassword(newPassword);

            check(memo);

            byte[] data1 = memo.Save();
            CollectionAssert.AreNotEqual(Src.Sample2Memo, data1);

            memo.Clear();

            IDPWMemo memo2 = new IDPWMemo();
            memo2.SetPassword(newPassword);
            memo2.LoadMemo(data1);

            check(memo2);

            memo2.ChangePassword(oldPassword);

            check(memo2);

            byte[] data2 = memo2.Save();
            CollectionAssert.AreEqual(Src.Sample2Memo, data2);
        }

        [TestMethod]
        public void TestServiceChanges()
        {
            IDPWMemo memo = new IDPWMemo();
            memo.SetPassword(Src.Password);
            memo.NewMemo();
            memo.AddNewService("xyz");
            Assert.IsFalse(memo.IsServiceEdited());
            memo.GetValues().Add(new Value(ValueType.Email, ""));
            Assert.IsFalse(memo.IsServiceEdited());
            memo.GetValues().Add(new Value(ValueType.Id, "abc"));
            Assert.IsTrue(memo.IsServiceEdited());
        }

        void check(IDPWMemo memo)
        {
            string[] names = Src.ServiceSetList.Select(ss => ss.Name).ToArray();
            CollectionAssert.AreEqual(names, memo.GetServiceNames());

            for (int i = 0; i < Src.ServiceSetList.Length; i++)
            {
                ServiceSrc src = Src.ServiceSetList[i];
                memo.SelectService(i);
                CollectionAssert.AreEqual(src.Values, memo.GetValues());
                CollectionAssert.AreEqual(src.Secrets, memo.GetSecrets());
            }
        }
    }

    class ServiceSrc
    {
        public readonly string Name;
        public readonly Value[] Values;
        public readonly Value[] Secrets;
        internal ServiceSrc(string n, Value[] v, Value[] s)
        {
            this.Name = n;
            this.Values = v;
            this.Secrets = s;
        }
    }

    static class Src
    {
        internal static readonly byte[] Password = System.Text.Encoding.UTF8.GetBytes("さんぷるdayo");

        internal static readonly ServiceSrc[] ServiceSetList = {
            new ServiceSrc(
                "SAMPLE Service",
                new Value[4] {
                    new Value(ValueType.ServiceName, "SAMPLE Service"),
                    new Value(ValueType.Id, "user1"),
                    new Value(ValueType.ServiceUrl, "http://example.com"),
                    new Value(ValueType.Email, "user1@example.com")
                },
                new Value[1] {
                    new Value(ValueType.Password, "Password1")
                }
            ),
            new ServiceSrc(
                "サンプル！",
                new Value[3] {
                    new Value(ValueType.ServiceName, "サンプル！"),
                    new Value(ValueType.Id, "ゆーざ１"),
                    new Value(ValueType.Description, "さんぷるなんだよ")
                },
                new Value[3] {
                    new Value(ValueType.Password, "ぱすわーど１"),
                    new Value(ValueType.ReminderQuestion, "あなたの国籍は？"),
                    new Value(ValueType.ReminderAnswer, "日本！")
                }
            ),
            new ServiceSrc(
                "simple-sample",
                new Value[2] {
                    new Value(ValueType.ServiceName, "simple-sample"),
                    new Value(ValueType.Id, "UserX")
                },
                new Value[0]
            )
        };

        internal static readonly byte[] Sample1Memo = {
            136, 173, 116, 165, 68, 204, 83, 146, 46, 246, 99, 149, 105, 124, 25, 219, 104, 214,
            14, 224, 183, 218, 98, 40, 198, 103, 164, 24, 246, 151, 255, 121, 163, 66, 76, 237, 94,
            65, 87, 253, 136, 252, 165, 119, 235, 114, 9, 27, 58, 231, 52, 69, 136, 183, 114, 221,
            172, 142, 89, 24, 251, 167, 245, 154, 211, 243, 20, 169, 245, 236, 235, 190, 70, 97,
            12, 151, 2, 193, 191, 88, 75, 206, 53, 71, 102, 140, 61, 187, 190, 146, 218, 11, 28,
            225, 137, 180, 32, 37, 66, 149, 52, 248, 112, 204, 247, 26, 2, 203, 150, 79, 78, 32,
            83, 131, 147, 204, 85, 105, 165, 160, 137, 151, 62, 56, 125, 173, 251, 182, 224, 171,
            58, 86, 77, 135, 62, 197, 222, 244, 149, 41, 175, 91, 96, 35, 2, 143, 215, 190, 40,
            128, 83, 238, 171, 146, 181, 150, 189, 52, 95, 82, 61, 63, 42, 250, 60, 168, 210, 90,
            115, 211, 244, 226, 25, 66, 228, 104, 159, 9, 125, 240, 212, 239, 146, 31, 56, 172,
            213, 65, 218, 178, 205, 207, 163, 230, 142, 175, 102, 100, 197, 203, 106, 183, 224, 92,
            168, 153, 81, 28, 112, 16, 130, 42, 70, 74, 220, 232, 12, 120, 184, 3, 125, 71, 186,
            89, 141, 88, 191, 211, 63, 52, 145, 92, 115, 62, 108, 148, 127, 102, 199, 195, 169, 24,
            9, 55, 48, 73, 105, 95, 195, 9, 206, 150, 137, 32, 217, 192, 106, 152, 96, 37, 128,
            161, 70, 227, 64, 32, 57, 87, 152, 155, 21, 96, 122, 252, 227, 122, 37, 31, 37, 143,
            53, 134, 94, 70, 146, 142, 156, 200, 139, 143, 227, 41, 235, 179, 159, 181, 190, 150,
            86, 160, 86, 51, 73, 202, 56, 58, 25, 113, 13, 142, 4, 83, 238, 181, 186, 16, 237, 253,
            139, 225, 63, 136, 242, 87, 3, 202, 242, 122, 217, 20, 8, 69, 226, 14, 240, 246, 179,
            71,
        };

        internal static readonly byte[] Sample2Memo = {
            45, 220, 210, 211, 50, 250, 55, 13, 22, 9, 156, 165, 192, 59, 63, 40, 80, 220, 133, 44, 62, 186, 253, 136, 76, 161, 28, 117, 80, 90, 144, 254,
            234, 189, 156, 216, 229, 133, 53, 251, 174, 175, 193, 222, 42, 180, 75, 181, 85, 93, 120, 172, 209, 139, 40, 37, 31, 169, 62, 169, 216, 98, 128, 54,
            37, 147, 216, 220, 124, 75, 199, 254, 145, 88, 191, 217, 34, 135, 63, 231, 201, 253, 243, 89, 185, 141, 35, 13, 58, 213, 68, 178, 225, 240, 21, 131,
            85, 171, 245, 6, 171, 82, 21, 183, 191, 207, 108, 247, 140, 125, 65, 217, 244, 233, 147, 182, 65, 150, 63, 179, 71, 8, 126, 177, 177, 178, 205, 60,
            63, 23, 232, 143, 192, 115, 207, 172, 81, 90, 195, 141, 186, 201, 36, 63, 253, 155, 36, 225, 246, 224, 180, 74, 120, 130, 79, 161, 40, 187, 199, 172,
            218, 177, 112, 47, 178, 138, 135, 6, 115, 35, 40, 222, 86, 148, 58, 35, 24, 195, 90, 245, 116, 74, 71, 214, 205, 244, 129, 236, 84, 180, 54, 116,
            230, 253, 138, 137, 8, 227, 138, 108, 178, 185, 56, 252, 20, 164, 165, 173, 79, 44, 128, 180, 136, 2, 20, 130, 154, 232, 59, 250, 161, 202, 11, 224,
            191, 83, 136, 8, 198, 103, 52, 208, 169, 13, 130, 85, 177, 40, 171, 30, 118, 206, 68, 168, 65, 209, 18, 40, 183, 96, 172, 199, 246, 35, 144, 178,
            139, 189, 179, 63, 78, 173, 63, 81, 34, 66, 253, 129, 143, 40, 195, 18, 215, 110, 47, 165, 236, 146, 198, 22, 100, 14, 183, 253, 220, 77, 111, 167,
            46, 215, 148, 163, 236, 179, 55, 67, 159, 189, 109, 65, 24, 1, 12, 61, 84, 97, 117, 34, 32, 239, 108, 227, 174, 227, 83, 28, 247, 15, 0, 11,
            205, 76, 247, 60, 6, 117, 23, 159, 16, 129, 151, 90, 37, 207, 99, 64, 58, 3, 167, 28, 105, 11, 34, 202, 123, 152, 235, 1, 95, 17, 66, 107,
            118, 53, 227, 86, 72, 2, 206, 36, 154, 87, 210, 167, 110,
        };
    }
}