﻿using System;

namespace Idpwmemo
{
    public sealed class IDPWMemoException : Exception
    {
        public IDPWMemoException()
        {
        }

        public IDPWMemoException(string message)
            : base(message)
        {
        }

        public IDPWMemoException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}