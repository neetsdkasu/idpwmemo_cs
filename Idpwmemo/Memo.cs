﻿using JavaDataIO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Idpwmemo
{
    public class Memo
    {
        const int Version = 2;

        private int _loadedVersion;
        private List<Service> _services;

        internal Memo() : this(Memo.Version, new List<Service>())
        {
        }

        internal Memo(int loadedVersion, List<Service> services)
        {
            _loadedVersion = loadedVersion;
            _services = services;
        }

        internal int LoadVersion
        {
            get
            {
                return _loadedVersion;
            }
        }

        internal int ServiceCount
        {
            get
            {
                return _services?.Count ?? 0;
            }
        }

        internal List<Service> Services
        {
            get
            {
                return _services;
            }
            set
            {
                _services = value ?? new List<Service>();
            }
        }

        internal int AddService(Service newService)
        {
            _services.Add(newService);
            return _services.Count - 1;
        }

        internal Service GetService(int index)
        {
            return _services[index];
        }

        internal void SetService(int index, Service service)
        {
            this._services[index] = service;
        }

        internal void RemoveService(int index)
        {
            _services.RemoveAt(index);
        }

        internal static Memo Load(JavaDataInput src)
        {
            int version = src.ReadInt();
            if (version > Memo.Version)
            {
                throw new IDPWMemoException("Invalid Version");
            }
            int count = src.ReadInt();
            List<Service> services = new List<Service>(count);
            if (version == 1)
            {
                for (int i = 0; i < count; i++)
                {
                    services.Add(Service.LoadV1(src));
                }
            }
            else
            {
                for (int i = 0; i < count; i++)
                {
                    services.Add(Service.Load(src));
                }
            }
            return new Memo(version, services);
        }

        internal List<Service> GetFilteredServices()
        {
            return _services.Where(s => s.ValidState).ToList();
        }

        internal void Save(JavaDataOutput dst)
        {
            dst.WriteInt(Memo.Version);
            List<Service> filtered = this.GetFilteredServices();
            dst.WriteInt(filtered.Count);
            foreach (Service s in filtered)
            {
                s.Save(dst);
            }
        }
    }
}