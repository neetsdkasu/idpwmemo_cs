﻿using JavaDataIO;
using System;

namespace Idpwmemo
{
    public class Value
    {
        public ValueType Type;
        public string Data;

        public Value(ValueType vType, string data)
        {
            Type = vType;
            Data = data;
        }

        public string TypeName
        {
            get
            {
                return Type.TypeName();
            }
        }

        public void Set(ValueType vType, string data)
        {
            Type = vType;
            Data = data;
        }

        public bool IsEmpty
        {
            get
            {
                return String.IsNullOrEmpty(Data);
            }
        }

        public Value Copy()
        {
            return (Value)this.MemberwiseClone();
        }

#if (NET5_0 || NETSTANDARD2_1)
        public override int GetHashCode()
        {
            return HashCode.Combine(Type, Data);
        }
#else
        public override int GetHashCode()
        {
            return Tuple.Create(Type, Data).GetHashCode();
        }
#endif

        public bool Equals(Value other)
        {
            if (other == null)
            {
                return false;
            }
            return (this.Type == other.Type) && (this.Data == other.Data);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Value);
        }

        public override string ToString()
        {
            return $"Value{{ Type:{Type}, Data:{Data} }}";
        }

        public static Value Load(JavaDataInput jdi)
        {
            var vType = (ValueType)jdi.ReadByte();
            string data = jdi.ReadUTF();
            return new Value(vType, data);
        }

        public void Save(JavaDataOutput jdo)
        {
            jdo.WriteByte((byte)Type);
            jdo.WriteUTF(Data);
        }
    }
}