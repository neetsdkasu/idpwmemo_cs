﻿using System;
using System.IO;
using Unkocrypto;

namespace Idpwmemo
{
    static class Cryptor
    {
        const int Version = 2;
        static readonly int s_maxBlockSize = Math.Min(1024, Crypto.MaxBlockSize);

        private static readonly MTRandom s_rand = new MTRandom();
        private static readonly Checksum s_cs = new CRC32();
        private static readonly object s_syncLock = new Object();

        internal static byte[] GetBytes(string s)
        {
            return System.Text.Encoding.UTF8.GetBytes(s);
        }

        private static uint[] GenSeedV1(int size)
        {
            uint[] seed = new uint[size];
            seed[0] = 0x98765432u;
            seed[1] = 0xF1E2D3C4u;
            for (int i = 2; i < seed.Length; i++)
            {
                seed[i] = seed[i - 2] ^ (seed[i - 1] >> ((i - 1) & 0xF)) ^ (seed[i - 1] << ((i + i + 1) & 0xF));
            }
            return seed;
        }

        private static uint[] GenSeedV1(byte[] password)
        {
            if (password == null || password.Length == 0)
            {
                return GenSeedV1(23);
            }
            uint[] seed = GenSeedV1(password.Length + 13);
            int p = 0;
            for (int i = 0; i < seed.Length; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    seed[i] |= unchecked((uint)((int)((sbyte)password[p]) << (((j + i * i) & 3) << 3)));
                    p++;
                    if (p >= password.Length)
                    {
                        p = 0;
                    }
                }
            }
            return seed;
        }

        private static int EncryptBlockSize(int srclen)
        {
            int size = Math.Max(Crypto.MinBlockSize, srclen + Crypto.MetaSize);
            if (size <= s_maxBlockSize)
            {
                return size;
            }
            size = Crypto.MinBlockSize;
            int blockCount = (srclen + (size - Crypto.MetaSize) - 1) / (size - Crypto.MetaSize);
            int totalSize = size * blockCount;
            for (int sz = Crypto.MinBlockSize + 1; sz <= s_maxBlockSize; sz++)
            {
                blockCount = (srclen + (sz - Crypto.MetaSize) - 1) / (sz - Crypto.MetaSize);
                if (sz * blockCount < totalSize)
                {
                    size = sz;
                    totalSize = sz * blockCount;
                }
            }
            return size;
        }

        internal static byte[] DecryptV1(string password, byte[] src)
        {
            return DecryptV1(GetBytes(password), src);
        }

        internal static byte[] DecryptV1(byte[] password, byte[] src)
        {
            lock (s_syncLock)
            {
                MemoryStream inStm = new MemoryStream(src);
                MemoryStream outStm = new MemoryStream();
                uint[] seed = GenSeedV1(password);
                for (int size = Math.Min(src.Length, s_maxBlockSize); size >= Crypto.MinBlockSize; size--)
                {
                    if (src.Length % size != 0)
                    {
                        continue;
                    }
                    inStm.Position = 0;
                    outStm.Position = 0;
                    outStm.SetLength(0);
                    s_rand.SetSeed(seed);
                    try
                    {
                        Crypto.Decrypt(size, s_cs, s_rand, inStm, outStm);
                        seed = null;
                        byte[] ret = outStm.ToArray();
                        inStm.Close();
                        outStm.Close();
                        return ret;
                    }
                    catch (CryptoException)
                    {
                        // continue;
                    }
                }
                seed = null;
                inStm.Close();
                outStm.Close();
                return null;
            }
        }

        internal static byte[] EncryptV1(string password, byte[] src)
        {
            return EncryptV1(GetBytes(password), src);
        }

        internal static byte[] EncryptV1(byte[] password, byte[] src)
        {
            lock (s_syncLock)
            {
                int blockSize = EncryptBlockSize(src.Length);
                MemoryStream inStm = new MemoryStream(src);
                MemoryStream outStm = new MemoryStream();
                s_rand.SetSeed(GenSeedV1(password));
                Crypto.Encrypt(blockSize, s_cs, s_rand, inStm, outStm);
                byte[] ret = outStm.ToArray();
                inStm.Close();
                outStm.Close();
                return ret;
            }
        }

        internal static byte[] DecryptRepeatV1(int times, string password, byte[] src)
        {
            return DecryptRepeatV1(times, GetBytes(password), src);
        }

        internal static byte[] DecryptRepeatV1(int times, byte[] password, byte[] src)
        {
            for (int i = 0; i < times; i++)
            {
                src = DecryptV1(password, src);
                if (src == null)
                {
                    return null;
                }
            }
            return src;
        }

        internal static byte[] EncryptRepeatV1(int times, string password, byte[] src)
        {
            return EncryptRepeatV1(times, GetBytes(password), src);
        }

        internal static byte[] EncryptRepeatV1(int times, byte[] password, byte[] src)
        {
            for (int i = 0; i < times; i++)
            {
                src = EncryptV1(password, src);
            }
            return src;
        }

        private static uint[] GenSeedV2(int size)
        {
            uint[] seed = new uint[size];
            seed[0] = 0x98765432u;
            seed[1] = 0xF1E2D3C4u;
            for (int i = 2; i < seed.Length; i++)
            {
                seed[i] = seed[i - 2] ^ (seed[i - 1] >> ((i - 1) & 0xF)) ^ (seed[i - 1] << ((i + i + 1) & 0xF));
            }
            return seed;
        }

        private static uint[] GenSeedV2(byte[] password)
        {
            if (password == null || password.Length == 0)
            {
                return GenSeedV2(37);
            }
            uint[] seed = GenSeedV2(password.Length + 29);
            int p = 0;
            for (int i = 0; i < seed.Length; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    seed[i] ^= unchecked((uint)((0xFF & (int)password[p]) << (((j + i * i) & 3) << 3)));
                    p++;
                    if (p >= password.Length)
                    {
                        p = 0;
                    }
                }
            }
            return seed;
        }

        internal static int CheckSrcType(byte[] src)
        {
            int srcType = 0;
            int version = 0;
            for (int i = 0; i < 8; i++)
            {
                version ^= (int)src[i] & 0xFF;
            }
            if (version == Cryptor.Version)
            {
                int dataLen = src.Length - 1;
                for (int size = Math.Min(dataLen, s_maxBlockSize); size >= Crypto.MinBlockSize; size--)
                {
                    if (dataLen % size == 0)
                    {
                        srcType = 2;
                        break;
                    }
                }
            }
            for (int size = Math.Min(src.Length, s_maxBlockSize); size >= Crypto.MinBlockSize; size--)
            {
                if (src.Length % size == 0)
                {
                    return srcType | 1;
                }
            }
            return srcType;
        }

        internal static byte[] DecryptV2(string password, byte[] src)
        {
            return DecryptV2(GetBytes(password), src);
        }

        internal static byte[] DecryptV2(byte[] password, byte[] src)
        {
            lock (s_syncLock)
            {
                if (src.Length < Crypto.MinBlockSize + 1)
                {
                    return null;
                }
                int version = 0;
                for (int i = 0; i < 8; i++)
                {
                    version ^= (int)src[i] & 0xFF;
                }
                if (version != Cryptor.Version)
                {
                    return null;
                }
                MemoryStream in1 = new MemoryStream(src);
                MemoryStream out1 = new MemoryStream();
                MemoryStream out2 = new MemoryStream();
                uint[] seed = GenSeedV2(password);
                int dataLen = src.Length - 1;
                for (int size1 = Math.Min(dataLen, s_maxBlockSize); size1 >= Crypto.MinBlockSize; size1--)
                {
                    if (dataLen % size1 != 0)
                    {
                        continue;
                    }
                    in1.Position = 1;
                    out1.Position = 0;
                    out1.SetLength(0);
                    s_rand.SetSeed(seed);
                    byte[] tmp = null;
                    try
                    {
                        Crypto.Decrypt(size1, s_cs, s_rand, in1, out1);
                        tmp = out1.ToArray();
                    }
                    catch (CryptoException)
                    {
                        continue;
                    }
                    MemoryStream in2 = new MemoryStream(tmp);
                    for (int size2 = Math.Min(tmp.Length, s_maxBlockSize); size2 >= Crypto.MinBlockSize; size2--)
                    {
                        if (tmp.Length % size2 != 0)
                        {
                            continue;
                        }
                        in2.Position = 0;
                        out2.Position = 0;
                        out2.SetLength(0);
                        s_rand.SetSeed(seed);
                        try
                        {
                            Crypto.Decrypt(size2, s_cs, s_rand, in2, out2);
                            byte[] ret = out2.ToArray();
                            seed = null;
                            tmp = null;
                            in1.Close(); in1 = null;
                            in2.Close(); in2 = null;
                            out1.Close(); out1 = null;
                            out2.Close(); out2 = null;
                            return ret;
                        }
                        catch (CryptoException)
                        {
                            // continue;
                        }
                    }
                    tmp = null;
                    in2.Close(); in2 = null;
                }
                seed = null;
                in1.Close(); in1 = null;
                out1.Close(); out1 = null;
                out2.Close(); out2 = null;
                return null;
            }
        }

        static int CalcSize(int srclen, int blockSize)
        {
            int dataSize = blockSize - Crypto.MetaSize;
            int blockCount = (srclen + dataSize - 1) / dataSize;
            return blockSize * blockCount;
        }

        internal static byte[] EncryptV2(string password, byte[] src)
        {
            return EncryptV2(GetBytes(password), src);
        }

        internal static byte[] EncryptV2(byte[] password, byte[] src)
        {
            lock (s_syncLock)
            {
                int blockSize = EncryptBlockSize(src.Length);
                // int size = CalcSize(src.Length, blockSize);
                uint[] seed = GenSeedV2(password);
                MemoryStream inStm = new MemoryStream(src);
                MemoryStream outStm = new MemoryStream();
                s_rand.SetSeed(seed);
                Crypto.Encrypt(blockSize, s_cs, s_rand, inStm, outStm);
                src = outStm.ToArray();
                inStm.Close();
                outStm.Close();
                blockSize = EncryptBlockSize(src.Length);
                // size = CalcSize(src.Length, blockSize);
                inStm = new MemoryStream(src);
                outStm = new MemoryStream();
                outStm.WriteByte(unchecked((byte)Cryptor.Version));
                s_rand.SetSeed(seed);
                Crypto.Encrypt(blockSize, s_cs, s_rand, inStm, outStm);
                byte[] ret = outStm.ToArray();
                inStm.Close();
                outStm.Close();
                for (int i = 1; i < 8; i++)
                {
                    ret[0] = unchecked((byte)((int)ret[0] ^ (int)ret[i]));
                }
                return ret;
            }
        }
    }
}