﻿using JavaDataIO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Idpwmemo
{
    public class Service
    {
        private long _time;
        private List<Value> _values;
        private byte[] _secrets;

        public Service(string serviceName)
            : this(new List<Value>(new Value[] {
                new Value(ValueType.ServiceName, serviceName)
            }), new byte[0])
        {
        }

        Service(List<Value> values, byte[] secrets) : this(0L, values, secrets)
        {
        }

        Service(long time, List<Value> values, byte[] secrets)
        {
            _time = time;
            _values = values;
            _secrets = secrets;
        }

        private Service() : this(0L, new List<Value>(), new byte[0])
        {
        }

        public long Time
        {
            get
            {
                return _time;
            }
            set
            {
                _time = value;
            }
        }

        public string GetServiceName()
        {
            foreach (Value v in _values)
            {
                if (v.Type == ValueType.ServiceName)
                {
                    return v.Data;
                }
            }
            return "";
        }

        public bool ValidState
        {
            get
            {
                return !String.IsNullOrEmpty(GetServiceName().Trim());
            }
        }

        public override string ToString()
        {
            return GetServiceName();
        }

        public Service Copy()
        {
            Service s = new Service();
            s._time = this._time;
            if ((this._values?.Count ?? 0) > 0)
            {
                s._values = this._values.Select(v => v.Copy()).ToList();
            }
            if ((this._secrets?.Length ?? 0) > 0)
            {
                s._secrets = (byte[])this._secrets.Clone();
            }
            return s;
        }

        public List<Value> Values
        {
            get
            {
                return _values;
            }
            set
            {
                _values = value ?? new List<Value>();
            }
        }

        public byte[] Secrets
        {
            get
            {
                return _secrets;
            }
            set
            {
                this._secrets = value ?? new byte[0];
            }
        }

        public bool HasSecrets
        {
            get
            {
                return (_secrets?.Length ?? 0) > 0;
            }
        }

        internal bool EqualsValues(List<Value> values)
        {
            if (this._values == null || values == null)
            {
                return Object.ReferenceEquals(this._values, values);
            }
            List<Value> lValues = Service.FilterValues(this._values);
            List<Value> rValues = Service.FilterValues(values);
            return Enumerable.SequenceEqual(lValues, rValues);
        }

        internal bool EqualsSecrets(byte[] secrets)
        {
            if (this._secrets == null || secrets == null)
            {
                return Object.ReferenceEquals(this._secrets, secrets);
            }
            return Enumerable.SequenceEqual(this._secrets, secrets);
        }

        public static bool Equals(Service s1, Service s2)
        {
            return s1.Time == s2.Time &&
                s1.EqualsValues(s2._values) &&
                s1.EqualsSecrets(s2._secrets);
        }

        internal static List<Value> FilterValues(List<Value> values)
        {
            return values?.Where(v => !v.IsEmpty).ToList() ?? new List<Value>();
        }

        internal static List<Value> ReadSecrets(JavaDataInput src)
        {
            List<Value> values;
            int vlen = src.ReadInt();
            values = new List<Value>(vlen);
            for (int i = 0; i < vlen; i++)
            {
                values.Add(Value.Load(src));
            }
            return values;
        }

        internal static void WriteSecrets(JavaDataOutput dst, List<Value> values)
        {
            dst.WriteInt(values.Count);
            foreach (Value v in values)
            {
                v.Save(dst);
            }
        }

        internal static Service LoadV1(JavaDataInput src)
        {
            List<Value> values;
            byte[] secrets;

            int vlen = src.ReadInt();
            values = new List<Value>(vlen);
            for (int i = 0; i < vlen; i++)
            {
                values.Add(Value.Load(src));
            }

            int slen = src.ReadInt();
            secrets = new byte[slen];
            src.ReadFully(secrets);

            return new Service(values, secrets);
        }

        internal static Service Load(JavaDataInput src)
        {
            long time;
            List<Value> values;
            byte[] secrets;

            time = src.ReadLong();

            int vlen = src.ReadInt();
            values = new List<Value>(vlen);
            for (int i = 0; i < vlen; i++)
            {
                values.Add(Value.Load(src));
            }

            int slen = src.ReadInt();
            secrets = new byte[slen];
            src.ReadFully(secrets);

            return new Service(time, values, secrets);
        }

        internal void Save(JavaDataOutput dst)
        {
            dst.WriteLong(Time);

            List<Value> filtered = Service.FilterValues(_values);
            dst.WriteInt(filtered.Count);
            foreach (Value v in filtered)
            {
                v.Save(dst);
            }

            dst.WriteInt(_secrets.Length);
            dst.Write(_secrets);
        }
    }
}