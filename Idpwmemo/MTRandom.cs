﻿using Mt19937ar;
using System;

namespace Idpwmemo
{
    sealed class MTRandom : Unkocrypto.IntRNG
    {
        private MersenneTwister mt = new MersenneTwister();

        internal void SetSeed(uint[] seed)
        {
            mt.SetSeed(seed);
        }

        public int NextInt()
        {
            return unchecked((int)mt.GenrandUint32());
        }
    }
}