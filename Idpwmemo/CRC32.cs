﻿namespace Idpwmemo
{
    internal sealed class CRC32 : Unkocrypto.Checksum
    {
        private static readonly long[] s_table = new long[256];

        static CRC32()
        {
            for (int n = 0; n < 256; n++)
            {
                long c = (long)n;
                for (int k = 0; k < 8; k++)
                {
                    if ((c & 1L) > 0L)
                    {
                        c = 0xEDB8_8320L ^ (c >> 1);
                    }
                    else
                    {
                        c = c >> 1;
                    }
                }
                s_table[n] = c;
            }
        }

        private long _value = 0xFFFF_FFFFL;

        public void Reset()
        {
            _value = 0xFFFF_FFFFL;
        }

        public void Update(int b)
        {
            _value = s_table[unchecked((int)((_value ^ (long)b) & 0xFFL))] ^ (_value >> 8);
        }

        public long Value => _value ^ 0xFFFF_FFFFL;
    }
}