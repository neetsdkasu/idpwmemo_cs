﻿using System;

namespace Idpwmemo
{
    public enum ValueType : byte
    {
        ServiceName = 0,
        ServiceUrl = 1,
        Id = 2,
        Password = 3,
        Email = 4,
        ReminderQuestion = 5,
        ReminderAnswer = 6,
        Description = 7
    }

    public static class ValueTypeExtensions
    {
        public static string TypeName(this ValueType vType)
        {
            switch (vType)
            {
                case ValueType.ServiceName:
                    return "service name";
                case ValueType.ServiceUrl:
                    return "service url";
                case ValueType.Id:
                    return "id";
                case ValueType.Password:
                    return "password";
                case ValueType.Email:
                    return "email";
                case ValueType.ReminderQuestion:
                    return "reminder question";
                case ValueType.ReminderAnswer:
                    return "reminder answer";
                case ValueType.Description:
                    return "description";
                default:
                    return "unknown";
            }
        }
    }
}