﻿using JavaDataIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Idpwmemo
{
    public class IDPWMemo
    {
        private byte[] _encodedPasswordV1 = null;
        private byte[] _encodedPasswordV2 = null;
        private int _cryptorVersion = 0;
        private Memo _memo = null;
        private int _serviceIndex = -1;
        private Service _editingService = null;
        private List<Value> _secretValues = null;
        private readonly object _syncLock = new Object();

        public void Clear()
        {
            lock (_syncLock)
            {
                IDPWMemo.FillDummy(_encodedPasswordV1);
                IDPWMemo.FillDummy(_encodedPasswordV2);
                _encodedPasswordV1 = null;
                _encodedPasswordV2 = null;
                _cryptorVersion = 0;
                _memo = null;
                _serviceIndex = -1;
                _editingService = null;
                _secretValues = null;
            }
        }

        public void SetPassword(string password)
        {
            this.SetPassword(Cryptor.GetBytes(password));
        }

        public void SetPassword(byte[] password)
        {
            lock (_syncLock)
            {
                byte[] tmp1 = Cryptor.EncryptV1(new byte[0], password);
                byte[] tmp2 = Cryptor.EncryptV2(password, password);
                IDPWMemo.FillDummy(_encodedPasswordV1);
                IDPWMemo.FillDummy(_encodedPasswordV2);
                _encodedPasswordV1 = tmp1;
                _encodedPasswordV2 = tmp2;
                _cryptorVersion = 0;
                _memo = null;
                _serviceIndex = -1;
                _editingService = null;
                _secretValues = null;
            }
        }

        private byte[] GetPasswordV1()
        {
            if (_encodedPasswordV1 == null)
            {
                throw new IDPWMemoException("No Password");
            }
            return Cryptor.DecryptV1(new byte[0], _encodedPasswordV1);
        }

        public bool HasPassword
        {
            get
            {
                lock (_syncLock)
                {
                    return _encodedPasswordV2 != null;
                }
            }
        }

        public bool HasMemo
        {
            get
            {
                lock (_syncLock)
                {
                    return _memo != null;
                }
            }
        }

        public int SelectedServiceIndex
        {
            get
            {
                lock (_syncLock)
                {
                    return _serviceIndex;
                }
            }
        }

        public void NewMemo()
        {
            this.NewMemo(2);
        }

        public void NewMemo(int version)
        {
            lock (_syncLock)
            {
                if (version < 1 || version > 2)
                {
                    throw new IDPWMemoException("Wrong Version");
                }
                if (_encodedPasswordV2 == null)
                {
                    throw new IDPWMemoException("No Password");
                }
                this._cryptorVersion = version;
                this._memo = new Memo();
                this._serviceIndex = -1;
                this._editingService = null;
                this._secretValues = null;
            }
        }

        public bool LoadMemo(byte[] src)
        {
            lock (_syncLock)
            {
                byte[] buf = null;
                int st = Cryptor.CheckSrcType(src);
                if ((st & 2) != 0)
                {
                    buf = Cryptor.DecryptV2(_encodedPasswordV2, src);
                }
                if (buf != null)
                {
                    this._cryptorVersion = 2;
                }
                else if ((st & 1) != 0)
                {
                    byte[] password = this.GetPasswordV1();
                    buf = Cryptor.DecryptRepeatV1(2, password, src);
                    IDPWMemo.FillDummy(password);
                    password = null;
                    if (buf == null)
                    {
                        return false;
                    }
                    this._cryptorVersion = 1;
                }
                else
                {
                    return false;
                }
                MemoryStream mem = new MemoryStream(buf);
                buf = null;
                Memo tmp = Memo.Load(new JavaDataInput(mem));
                mem.Close();
                this._memo = tmp;
                this._serviceIndex = -1;
                this._editingService = null;
                this._secretValues = null;
                return true;
            }
        }

        // 現在のMemo内のServiceの数を返す
        public int ServiceCount
        {
            get
            {
                lock (_syncLock)
                {
                    if (_memo == null)
                    {
                        return 0;
                    }
                    return _memo.ServiceCount;
                }
            }
        }

        // 現在のMemo内のServiceリストの操作
        // ※setの場合はMemoのService全てが置き換えられるため
        //   現在編集中のServiceが破棄される
        public List<Service> Services
        {
            get
            {
                lock (_syncLock)
                {
                    if (_memo == null)
                    {
                        return null;
                    }
                    return _memo.Services;
                }
            }
            set
            {
                lock (_syncLock)
                {
                    if (_memo == null)
                    {
                        throw new IDPWMemoException("No Memo");
                    }
                    _memo.Services = value;
                    _serviceIndex = -1;
                    _editingService = null;
                    _secretValues = null;
                }
            }
        }

        // Memo内の指定インデックスのServiceを取得する
        public Service GetService(int index)
        {
            lock (_syncLock)
            {
                if (_memo == null)
                {
                    return null;
                }
                return _memo.GetService(index);
            }
        }

        // 編集中のServiceと同じIndexを持つMemo内のServiceを取得する
        public Service GetService()
        {
            lock (_syncLock)
            {
                if (_serviceIndex < 0)
                {
                    return null;
                }
                return _memo.GetService(_serviceIndex);
            }
        }

        // 編集中のServiceを取得
        // ※編集中のSecretValuesを編集中のServiceに詰める処理も行われる
        public Service GetSelectedService()
        {
            lock (_syncLock)
            {
                if (_editingService == null)
                {
                    return null;
                }
                this.SaveSecrets();
                return _editingService;
            }
        }

        // 編集中のServiceの名前を取得
        public string GetSelectedServiceName()
        {
            lock (_syncLock)
            {
                if (_editingService == null)
                {
                    return null;
                }
                return _editingService.GetServiceName();
            }
        }

        // 現在のMemo内のServiceの名前一覧を取得
        // ※編集中のServiceの情報は反映されていない
        public List<string> GetServiceNames()
        {
            lock (_syncLock)
            {
                if (_memo == null)
                {
                    return null;
                }
                return _memo.Services.Select(s => s.GetServiceName()).ToList();
            }
        }

        // 新しいServiceを現在のMemoの最後尾に追加する
        // 新しいServiceは他のIDPWMemoの編集中のServiceのコピーから生成
        // ※現在編集中のServiceは破棄され、コピーされたものが編集中扱いとなる
        public void AddService(IDPWMemo serviceFrom)
        {
            lock (_syncLock)
            {
                if (serviceFrom == null)
                {
                    throw new ArgumentNullException("serviceFrom");
                }
                if (_memo == null)
                {
                    throw new IDPWMemoException("No Memo");
                }
                _editingService = serviceFrom.GetSelectedService().Copy();
                _secretValues = serviceFrom.GetSecrets().Select(v => v.Copy()).ToList();
                this.SaveSecrets();
                _serviceIndex = _memo.AddService(_editingService.Copy());
                _secretValues = null;
            }
        }

        // 新しいServiceを現在のMemoの指定のindexに設定する(既存のindexのものと置き換える)
        // 新しいServiceは他のIDPWMemoの編集中のServiceのコピーから生成
        // ※現在編集中のServiceは破棄され、コピーされたものが編集中扱いとなる
        public void SetService(int index, IDPWMemo serviceFrom)
        {
            lock (_syncLock)
            {
                if (serviceFrom == null)
                {
                    throw new ArgumentNullException("serviceFrom");
                }
                if (_memo == null)
                {
                    throw new IDPWMemoException("No Memo");
                }
                if (index < 0 || index >= _memo.ServiceCount)
                {
                    throw new IndexOutOfRangeException();
                }
                _editingService = serviceFrom.GetSelectedService().Copy();
                _secretValues = serviceFrom.GetSecrets().Select(v => v.Copy()).ToList();
                this.SaveSecrets();
                _serviceIndex = index;
                _memo.SetService(index, _editingService.Copy());
                _secretValues = null;
            }
        }

        // 新しいServiceを現在のMemoの最後尾に追加する
        // 新しいServiceは指定された名前を持つだけのものになる
        // ※現在編集中のServiceは破棄され、新しいものが編集中扱いとなる
        public void AddNewService(string serviceName)
        {
            lock (_syncLock)
            {
                if (_memo == null)
                {
                    throw new IDPWMemoException("No Memo");
                }
                _editingService = new Service(serviceName);
                _serviceIndex = _memo.AddService(_editingService.Copy());
                _secretValues = null;
            }
        }

        // 編集中のServiceと元のServiceに違いがあるかを確認する
        public bool IsServiceEdited()
        {
            lock (_syncLock)
            {
                if (_memo == null)
                {
                    return false;
                }
                if (_editingService == null)
                {
                    return false;
                }
                this.SaveSecrets();
                return !Service.Equals(_editingService, _memo.GetService(_serviceIndex));
            }
        }


        // 編集中のServiceは破棄し、Serviceの選択を解除する
        public void UnselectService()
        {
            lock (_syncLock)
            {
                _serviceIndex = -1;
                _editingService = null;
                _secretValues = null;
            }
        }

        // 現在のMemoの指定indexのServiceのコピーを編集中に設定する
        // ※それまでの編集中のServiceは破棄される
        public void SelectService(int index)
        {
            if (index < 0)
            {
                this.UnselectService();
                return;
            }
            lock (_syncLock)
            {
                if (_memo == null)
                {
                    throw new IDPWMemoException("No Memo");
                }
                if (index >= _memo.ServiceCount)
                {
                    throw new IndexOutOfRangeException();
                }
                _serviceIndex = index;
                _editingService = _memo.GetService(index).Copy();
                _secretValues = null;
            }
        }

        // 現在編集中のServiceのValueリストの取得
        public List<Value> GetValues()
        {
            lock (_syncLock)
            {
                if (_editingService == null)
                {
                    return null;
                }
                return _editingService.Values;
            }
        }

        // 現在編集中のServiceのValueリストをまるごと置き換え
        public void SetValues(List<Value> values)
        {
            lock (_syncLock)
            {
                if (_editingService == null)
                {
                    throw new IDPWMemoException("Not Select Service");
                }
                _editingService.Values = values;
            }
        }

        // 現在編集中のSecretのValueリストをまるごと置き換え
        // ※編集中のServiceには反映されない
        public void SetSecrets(List<Value> secrets)
        {
            lock (_syncLock)
            {
                if (_editingService == null)
                {
                    throw new IDPWMemoException("Not Select Service");
                }
                _secretValues = secrets ?? new List<Value>();
            }
        }

        public List<Value> GetSecrets()
        {
            lock (_syncLock)
            {
                if (_secretValues != null)
                {
                    return _secretValues;
                }
                if (_editingService == null)
                {
                    return null;
                }
                byte[] src = _editingService.Secrets;
                if (src == null || src.Length == 0)
                {
                    _secretValues = new List<Value>();
                    return _secretValues;
                }
                byte[] buf = null;
                if (_cryptorVersion == 1)
                {
                    byte[] password = this.GetPasswordV1();
                    buf = Cryptor.DecryptRepeatV1(2, password, src);
                    IDPWMemo.FillDummy(password);
                    password = null;
                }
                else if (_cryptorVersion == 2)
                {
                    buf = Cryptor.DecryptV2(_encodedPasswordV2, src);
                }
                src = null;
                if (buf == null)
                {
                    throw new IDPWMemoException("Secrets Data Is Broken");
                }
                MemoryStream mem = new MemoryStream(buf);
                List<Value> tmp = Service.ReadSecrets(new JavaDataInput(mem));
                mem.Close();
                IDPWMemo.FillDummy(buf); // 何の意味が・・？
                buf = null;
                mem = null;
                _secretValues = tmp;
                tmp = null;
                return _secretValues;
            }
        }

        // 現在のMemoの指定indexのServiceを削除する
        // ※編集中のServiceの場合はそれも削除される
        public void RemoveService(int index)
        {
            if (_serviceIndex == index)
            {
                this.RemoveSelectedService();
                return;
            }
            lock (_syncLock)
            {
                if (_memo == null)
                {
                    throw new IDPWMemoException("No Memo");
                }
                if (index < 0 || index >= _memo.ServiceCount)
                {
                    throw new IndexOutOfRangeException();
                }
                if (index < _serviceIndex)
                {
                    _serviceIndex--;
                }
                _memo.RemoveService(index);
            }
        }

        // 編集中のServiceのデータをMemoの当該Serviceに反映させる
        public void UpdateSelectedService()
        {
            lock (_syncLock)
            {
                if (_editingService == null)
                {
                    throw new IDPWMemoException("Not Select Service");
                }
                this.SaveSecrets();
                _editingService.Time = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
                _memo.SetService(_serviceIndex, _editingService.Copy());
            }
        }

        // 現在のMemoの編集中のServiceの元のServiceを削除する
        // 編集中のSerivceも破棄する
        public void RemoveSelectedService()
        {
            lock (_syncLock)
            {
                if (_editingService == null)
                {
                    throw new IDPWMemoException("Not Select Service");
                }
                _memo.RemoveService(_serviceIndex);
                _serviceIndex = -1;
                _editingService = null;
                _secretValues = null;
            }
        }

        // 編集中のSecretのValueリストを編集中のServiceに反映させる
        // ※現在のMemo内のServiceには影響を与えない
        private void SaveSecrets()
        {
            if (_secretValues == null)
            {
                return;
            }
            List<Value> filtered = Service.FilterValues(_secretValues);
            if (filtered.Count == 0)
            {
                _editingService.Secrets = null;
                return;
            }
            MemoryStream mem = new MemoryStream();
            Service.WriteSecrets(new JavaDataOutput(mem), filtered);
            filtered = null;
            byte[] buf = mem.ToArray();
            mem.Close();
            if (_cryptorVersion == 1)
            {
                byte[] password = this.GetPasswordV1();
                _editingService.Secrets = Cryptor.EncryptRepeatV1(2, password, buf);
                IDPWMemo.FillDummy(password);
                password = null;
            }
            else if (_cryptorVersion == 2)
            {
                _editingService.Secrets = Cryptor.EncryptV2(_encodedPasswordV2, buf);
            }
            IDPWMemo.FillDummy(buf);
            buf = null;
        }

        // 現在のMemoを保存用byte列に変換する
        // ※編集中のServiceは反映されない
        public byte[] Save()
        {
            lock (_syncLock)
            {
                if (_memo == null)
                {
                    throw new IDPWMemoException("No Memo");
                }
                MemoryStream mem = new MemoryStream();
                _memo.Save(new JavaDataOutput(mem));
                byte[] buf = mem.ToArray();
                mem.Close();
                if (_cryptorVersion == 1)
                {
                    return Cryptor.EncryptRepeatV1(2, this.GetPasswordV1(), buf);
                }
                else if (_cryptorVersion == 2)
                {
                    return Cryptor.EncryptV2(_encodedPasswordV2, buf);
                }
                else
                {
                    throw new IDPWMemoException("BUG");
                }
            }
        }

        // 現在のMemoのパスワードを変更する
        public void ChangePassword(string newPassword)
        {
            this.ChangePassword(Cryptor.GetBytes(newPassword));
        }

        // 現在のMemoのパスワードを変更する
        public void ChangePassword(byte[] newPassword)
        {
            lock (_syncLock)
            {
                if (_memo == null)
                {
                    throw new IDPWMemoException("No Memo");
                }
                if (_cryptorVersion == 1)
                {
                    this.ChangePasswordV1(newPassword);
                }
                else if (_cryptorVersion == 2)
                {
                    this.ChangePasswordV2(newPassword);
                }
                else
                {
                    throw new IDPWMemoException("BUG");
                }
            }
        }

        // 現在のMemoのパスワードを変更する(パスワードがVersion1の場合)
        private void ChangePasswordV1(byte[] newPassword)
        {
            byte[] oldPassword = this.GetPasswordV1();
            byte[][] encs = new byte[_memo.ServiceCount][];
            for (int i = 0; i < _memo.ServiceCount; i++)
            {
                Service sv = _memo.GetService(i);
                if (!sv.HasSecrets)
                {
                    encs[i] = new byte[0];
                    continue;
                }
                byte[] sec = sv.Secrets;
                byte[] dec = Cryptor.DecryptRepeatV1(2, oldPassword, sec);
                sec = null;
                encs[i] = Cryptor.EncryptRepeatV1(2, newPassword, dec);
                IDPWMemo.FillDummy(dec);
                dec = null;
            }
            byte[] tmp1 = Cryptor.EncryptV1(new byte[0], newPassword);
            byte[] tmp2 = Cryptor.EncryptV2(newPassword, newPassword);
            if (_editingService != null && _editingService.HasSecrets)
            {
                byte[] sec = _editingService.Secrets;
                byte[] dec = Cryptor.DecryptRepeatV1(2, oldPassword, sec);
                sec = null;
                byte[] enc = Cryptor.EncryptRepeatV1(2, newPassword, dec);
                IDPWMemo.FillDummy(dec);
                dec = null;
                _editingService.Secrets = enc;
                enc = null;
            }
            _encodedPasswordV1 = tmp1;
            _encodedPasswordV2 = tmp2;
            for (int i = 0; i < _memo.ServiceCount; i++)
            {
                _memo.GetService(i).Secrets = encs[i];
            }
            IDPWMemo.FillDummy(oldPassword);
            oldPassword = null;
            tmp1 = null;
            tmp2 = null;
            encs = null;
        }

        // 現在のMemoのパスワードを変更する(パスワードがVersion2の場合)
        private void ChangePasswordV2(byte[] newPassword)
        {
            byte[] tmpPasswordV2 = Cryptor.EncryptV2(newPassword, newPassword);
            byte[][] encs = new byte[_memo.ServiceCount][];
            for (int i = 0; i < _memo.ServiceCount; i++)
            {
                Service sv = _memo.GetService(i);
                if (!sv.HasSecrets)
                {
                    encs[i] = new byte[0];
                    continue;
                }
                byte[] sec = sv.Secrets;
                byte[] dec = Cryptor.DecryptV2(_encodedPasswordV2, sec);
                sec = null;
                encs[i] = Cryptor.EncryptV2(tmpPasswordV2, dec);
                IDPWMemo.FillDummy(dec);
                dec = null;
            }
            byte[] tmpPasswordV1 = Cryptor.EncryptV1(new byte[0], newPassword);
            if (_editingService != null && _editingService.HasSecrets)
            {
                byte[] sec = _editingService.Secrets;
                byte[] dec = Cryptor.DecryptV2(_encodedPasswordV2, sec);
                sec = null;
                byte[] enc = Cryptor.EncryptV2(tmpPasswordV2, dec);
                IDPWMemo.FillDummy(dec);
                dec = null;
                _editingService.Secrets = enc;
                enc = null;
            }
            _encodedPasswordV1 = tmpPasswordV1;
            _encodedPasswordV2 = tmpPasswordV2;
            for (int i = 0; i < _memo.ServiceCount; i++)
            {
                _memo.GetService(i).Secrets = encs[i];
            }
            tmpPasswordV1 = null;
            tmpPasswordV2 = null;
            encs = null;
        }

        public void ConvertV1ToV2()
        {
            lock (_syncLock)
            {
                if (_cryptorVersion != 1)
                {
                    return;
                }
                byte[] oldPassword = this.GetPasswordV1();
                byte[][] encs = new byte[_memo.ServiceCount][];
                for (int i = 0; i < _memo.ServiceCount; i++)
                {
                    Service sv = _memo.GetService(i);
                    if (!sv.HasSecrets)
                    {
                        encs[i] = new byte[0];
                        continue;
                    }
                    byte[] sec = sv.Secrets;
                    byte[] dec = Cryptor.DecryptRepeatV1(2, oldPassword, sec);
                    sec = null;
                    encs[i] = Cryptor.EncryptV2(_encodedPasswordV2, dec);
                    IDPWMemo.FillDummy(dec);
                    dec = null;
                }
                if (_editingService != null && _editingService.HasSecrets)
                {
                    byte[] sec = _editingService.Secrets;
                    byte[] dec = Cryptor.DecryptRepeatV1(2, oldPassword, sec);
                    sec = null;
                    byte[] enc = Cryptor.EncryptV2(_encodedPasswordV2, dec);
                    IDPWMemo.FillDummy(dec);
                    dec = null;
                    _editingService.Secrets = enc;
                    enc = null;
                }
                for (int i = 0; i < _memo.ServiceCount; i++)
                {
                    _memo.GetService(i).Secrets = encs[i];
                }
                _cryptorVersion = 2;
                IDPWMemo.FillDummy(oldPassword);
                oldPassword = null;
                encs = null;
            }
        }

        // 気休め・・・まるで意味なさそう
        private static void FillDummy(byte[] buf)
        {
            if (buf == null)
            {
                return;
            }
            for (int i = 0; i < buf.Length; i++)
            {
                buf[i] ^= unchecked((byte)(0xEACFBD >> (i ^ 15)));
                unchecked
                {
                    buf[i] += buf[(i + 3) % buf.Length];
                }
            }
        }
    }

#if NET452
    static class DateTimeOffsetExtensions
    {
        internal static long ToUnixTimeMilliseconds(this DateTimeOffset dto)
        {
            var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var span = dto.Subtract(new DateTimeOffset(unixEpoch));
            return span.Ticks / TimeSpan.TicksPerMillisecond;
        }
    }
#endif
}